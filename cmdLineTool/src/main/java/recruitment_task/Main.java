package recruitment_task;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class Main {

	protected static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

	private static class CREATE_REQUEST {
		private static final String URL = "https://ej90p5q8o3.execute-api.us-east-2.amazonaws.com/prod";
		private static final String NAME_ARG = "name";
	}
	
	private static class ADD_README_REQUEST {
		private static final String URL = "https://euo6ls5szc.execute-api.us-east-2.amazonaws.com/prod";
		private static final String ID_ARG = "id";
		private static final String CONTENT = "readme";
	}
	
	private static class MERGE_README_REQUEST {
		private static final String URL = "https://to57ict5fb.execute-api.us-east-2.amazonaws.com/prod";
		private static final String LIST_OF_PROJECTS_ARG = "list_of_projects";
	}

	private static final String MERGE_READMES = "merge_readmes";
	private static final String ADD_README = "add_readme";
	private static final String CREATE_REPO = "create_repo";

	public static void main(String[] args) throws IOException {
		Options options = createOptions();

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd = null;

		String response = "";

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);
			System.exit(1);
		}

		if (cmd.hasOption(CREATE_REPO)) {
			if (cmd.hasOption("n")) {
				response = createrepo(cmd.getOptionValue("n"));
			} else {
				System.out.println("Missing repository Name");
			}
		} else if (cmd.hasOption(ADD_README)) {
			if (cmd.hasOption("id") && (cmd.hasOption("c") || cmd.hasOption("f"))) {
				String readme = ""; 
				if(cmd.hasOption("c"))
				{
					readme = cmd.getOptionValue("c");
				}
				else {
					Path fileName = Path.of(cmd.getOptionValue("f"));
					readme = Files.readString(fileName);
					
				}
				response = addReadme(cmd.getOptionValue("id"), readme);
			} else {
				System.out.println("Missing repo Name");
			}

		} else if (cmd.hasOption(MERGE_READMES)) {
			if(cmd.hasOption("l"))
			{
				response = mergeReadme(cmd.getOptionValue("l"));
			}
			else {
				System.out.println("Missing List of projects");
			}

		} else {
			formatter.printHelp(
					"use with either " + CREATE_REPO + ", " + ADD_README + " or " + MERGE_READMES + " option", options);
		}
		System.out.println(response);
	}
	private static Options createOptions() {
		Options options = new Options();

		Option createRepo = new Option(CREATE_REPO, false, "create repository, use with param name");
		Option addReadme = new Option(ADD_README, false, "add readme");
		Option mergeReadmes = new Option(MERGE_READMES, false, "merge readme files from repositories");
		Option fileName = new Option("f", "file_name", true, "file name");
		Option repoName = new Option("n", "name", true, "repository name");
		Option repoid = new Option("id", true, "repository id");
		Option content = new Option("c", "content", true, "file content");
		Option list = new Option("l", "list", true, "list of projects separated by ';' example: '112;234;634'");

		options.addOption(createRepo);
		options.addOption(addReadme);
		options.addOption(mergeReadmes);
		options.addOption(fileName);
		options.addOption(repoName);
		options.addOption(repoid);
		options.addOption(content);
		options.addOption(list);
		return options;
	}

	private static String createrepo(String name) {
		try (CloseableHttpClient client = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(CREATE_REQUEST.URL);

			JsonObject json = new JsonObject();
			json.addProperty(CREATE_REQUEST.NAME_ARG, name);
			StringEntity entity = new StringEntity(gson.toJson(json));
			httpPost.setEntity(entity);

			HttpResponse httpResponse = client.execute(httpPost);

			return getResponseAsString(httpResponse);

		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	private static String addReadme(String id, String readme) {
		try (CloseableHttpClient client = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(ADD_README_REQUEST.URL);

			JsonObject json = new JsonObject();
			json.addProperty(ADD_README_REQUEST.ID_ARG, id);
			json.addProperty(ADD_README_REQUEST.CONTENT, readme);
			StringEntity entity = new StringEntity(gson.toJson(json));
			httpPost.setEntity(entity);

			HttpResponse httpResponse = client.execute(httpPost);

			return getResponseAsString(httpResponse);

		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	private static String mergeReadme(String listOfProjects) {
		try (CloseableHttpClient client = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(MERGE_README_REQUEST.URL);

			JsonObject json = new JsonObject();
			json.addProperty(MERGE_README_REQUEST.LIST_OF_PROJECTS_ARG, listOfProjects);
			StringEntity entity = new StringEntity(gson.toJson(json));
			httpPost.setEntity(entity);

			HttpResponse httpResponse = client.execute(httpPost);

			return getResponseAsString(httpResponse);

		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	private static String getResponseAsString(HttpResponse httpResponse) throws IOException {
		String json = EntityUtils.toString(httpResponse.getEntity()).replace("\\n", "\n").replace("\\\"", "\"");
		return json;
	}
}
