package recrutmenttest;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import software.amazon.awssdk.services.lambda.LambdaAsyncClient;
import software.amazon.awssdk.services.lambda.model.GetAccountSettingsRequest;
import software.amazon.awssdk.services.lambda.model.GetAccountSettingsResponse;

// Handler value: example.Handler
public class Handler implements RequestHandler<Map<String, String>, String> {
	private static final String PRIVATE_TOKEN = "VenJQT3y45-VonbQqHz3";
	private static final Logger logger = LoggerFactory.getLogger(Handler.class);
	private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private static final LambdaAsyncClient lambdaClient = LambdaAsyncClient.create();
	private static final String GITLAB_URL = "";

	public Handler() {
		CompletableFuture<GetAccountSettingsResponse> accountSettings = lambdaClient
				.getAccountSettings(GetAccountSettingsRequest.builder().build());
		try {
			GetAccountSettingsResponse settings = accountSettings.get();
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Override
	public String handleRequest(Map<String,String> event, Context context) {
		
		String response;
		String repoName = event.get("name");
		String readme = event.get("readme");
		
		
		if(repoName == null || "".equals(repoName))
		{
			response = "invalid repo name";
		}

		try {
			createGitLabRepo(repoName);
			updateReadme(readme);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//    // call Lambda API
//    logger.info("Getting account settings");
//    CompletableFuture<GetAccountSettingsResponse> accountSettings = 
//        lambdaClient.getAccountSettings(GetAccountSettingsRequest.builder().build());
//	// log execution details
//    logger.info("ENVIRONMENT VARIABLES: {}", gson.toJson(System.getenv()));
//    logger.info("CONTEXT: {}", gson.toJson(context));
//    logger.info("EVENT: {}", gson.toJson(event));
//    // process event
//    
//    gson.to
//    String json = gson.toJson(event);
//    
//    for(SQSMessage msg : event.getRecords()){
//      logger.info(msg.getBody());
//    }
//    // process Lambda API response
//    try {
//      GetAccountSettingsResponse settings = accountSettings.get();
//      response = gson.toJson(settings.accountUsage());
//      logger.info("Account usage: {}", response);
//    } catch(Exception e) {
//      e.getStackTrace();
//    }
		return response;
	}

	private void createGitLabRepo(String repoName) throws IOException {

		URL url = new URL(GITLAB_URL);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("PRIVATE-TOKEN", PRIVATE_TOKEN);

		Map<String, String> parameters = new HashMap<>();
		parameters.put("name", repoName);
		parameters.put("initialize_with_readme", "true");

		con.setDoOutput(true);

		DataOutputStream out = new DataOutputStream(con.getOutputStream());
		out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
		out.flush();
		out.close();
	}

}

class ParameterStringBuilder {
	public static String getParamsString(Map<String, String> params) throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();

		for (Map.Entry<String, String> entry : params.entrySet()) {
			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
			result.append("&");
		}

		String resultString = result.toString();
		return resultString.length() > 0 ? resultString.substring(0, resultString.length() - 1) : resultString;
	}
}
