package recrutmenttest;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import software.amazon.awssdk.services.lambda.LambdaAsyncClient;
import software.amazon.awssdk.services.lambda.model.GetAccountSettingsRequest;
import software.amazon.awssdk.services.lambda.model.GetAccountSettingsResponse;

public abstract class AwsLambdaHandler  implements RequestHandler<Map<String, String>, String>{

	private static final LambdaAsyncClient lambdaClient = LambdaAsyncClient.create();
	protected static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	public AwsLambdaHandler() {
		CompletableFuture<GetAccountSettingsResponse> accountSettings = lambdaClient
				.getAccountSettings(GetAccountSettingsRequest.builder().build());
		try {
			GetAccountSettingsResponse settings = accountSettings.get();
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

}
