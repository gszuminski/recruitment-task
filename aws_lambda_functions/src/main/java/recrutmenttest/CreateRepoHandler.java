package recrutmenttest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.amazonaws.services.lambda.runtime.Context;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

// Handler value: example.Handler
public class CreateRepoHandler extends AwsLambdaHandler 
{

	private static final String INPUT_ARG_NAME = "name";

	private static class REQUEST 
	{
		private static final String URL = "https://gitlab.com/api/v4/projects";
		
		private static final String PRIVATE_TOKEN_ARG = "PRIVATE-TOKEN";
		private static final String NAME_ARG = "branch";
		
		private static final String PRIVATE_TOKEN = "VenJQT3y45-VonbQqHz3";
	}
	
	
	public CreateRepoHandler() {
		super();
	}

	public String handleRequest(Map<String, String> event, Context context) {

		final String repoName = event.get(INPUT_ARG_NAME);

		if (repoName == null || "".equals(repoName)) {
			return "Missing repository name";
		} else {
			return createGitLabRepo(repoName);
		}
	}

	private static String createGitLabRepo(String repoName) {
		try (CloseableHttpClient client = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(REQUEST.URL);
			httpPost.addHeader(REQUEST.PRIVATE_TOKEN_ARG, REQUEST.PRIVATE_TOKEN);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(REQUEST.NAME_ARG, repoName));

			httpPost.setEntity(new UrlEncodedFormEntity(params));

			HttpResponse httpResponse = client.execute(httpPost);
			String json = EntityUtils.toString(httpResponse.getEntity());
			JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
			return gson.toJson(jsonObject);
		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}


