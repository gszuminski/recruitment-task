package recrutmenttest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.amazonaws.services.lambda.runtime.Context;

// Handler value: example.Handler
public class MergeRedeameHandler extends AwsLambdaHandler {

	private static final String INPUT_ARG_LIST_OF_PROJECTS = "list_of_projects";

	private static class REQUEST 
	{
		private static final String ID_MARKER = "__ID__";
		private static final String URL = "https://gitlab.com/api/v4/projects/" + ID_MARKER + "/repository/files/readme%2Emd/raw";
		
		private static final String PRIVATE_TOKEN_ARG = "PRIVATE-TOKEN";
		private static final String REF_ARG = "ref";
		
		private static final String PRIVATE_TOKEN = "VenJQT3y45-VonbQqHz3";
	}
	
	public MergeRedeameHandler() {
		super();
	}

	public String handleRequest(Map<String, String> event, Context context) {

		final String listOfProjects = event.get(INPUT_ARG_LIST_OF_PROJECTS);

		if (listOfProjects == null || "".equals(listOfProjects)) {
			return "missing projects";
		} else {
			String[] projects = listOfProjects.split(";");
			return readFiles(projects);
		}
	}

	private static String readFiles(String[] projects) {
		StringBuilder mergedContent = new StringBuilder();
		try {
			for (String project : projects) {
				mergedContent.append(getReadmeFromProject(project)).append("\n");
			}
		} catch (IOException | URISyntaxException e) {
			return e.getMessage();
		}
		return mergedContent.toString();
	}

	private static String getReadmeFromProject(String project) throws IOException, URISyntaxException {
		try (CloseableHttpClient client = HttpClients.createDefault()) {
			URIBuilder builder = new URIBuilder(REQUEST.URL.replace(REQUEST.ID_MARKER, project));
			builder.setParameter(REQUEST.REF_ARG, "master");
			HttpGet httpGet = new HttpGet(builder.build());
			System.out.println(builder.build().toString());
			
			httpGet.addHeader(REQUEST.PRIVATE_TOKEN_ARG, REQUEST.PRIVATE_TOKEN);
			HttpResponse httpResponse = client.execute(httpGet);
			String result = EntityUtils.toString(httpResponse.getEntity());
			return result;
		}
	}
}