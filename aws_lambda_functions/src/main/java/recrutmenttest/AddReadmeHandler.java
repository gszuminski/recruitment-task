package recrutmenttest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.amazonaws.services.lambda.runtime.Context;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class AddReadmeHandler extends AwsLambdaHandler {
	

	private static final String INPUT_ARG_README = "readme";
	private static final String INPUT_ARG_ID = "id";
	
	private static class REQUEST 
	{
		private static final String ID_MARKER = "__ID__";
		private static final String URL = "https://gitlab.com/api/v4/projects/" + ID_MARKER + "/repository/files/readme%2Emd";
		
		private static final String PRIVATE_TOKEN_ARG = "PRIVATE-TOKEN";
		private static final String BRANCH_ARG = "branch";
		private static final String COMMIT_MESSAGE_ARG = "commit_message";
		private static final String CONTENT_ARG = "content";
		
		private static final String PRIVATE_TOKEN = "VenJQT3y45-VonbQqHz3";
	}
	
	public AddReadmeHandler() {
		super();
	}

	@Override
	public String handleRequest(Map<String, String> event, Context context) {

		final String repoId = event.get(INPUT_ARG_ID);
		final String readmeBody = event.get(INPUT_ARG_README);
		
		if (repoId == null || "".equals(repoId)) {
			return "Missing repository name";
		} else if (readmeBody == null || "".equals(readmeBody)) {
			return "Missing readme body";
		}
		return createReadme(repoId, readmeBody);
	}

	private static String createReadme(String repoId, String readmeBody) {
		try (CloseableHttpClient client = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(REQUEST.URL.replace(REQUEST.ID_MARKER,repoId));
			httpPost.addHeader(REQUEST.PRIVATE_TOKEN_ARG, REQUEST.PRIVATE_TOKEN);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(REQUEST.BRANCH_ARG, "master"));
			params.add(new BasicNameValuePair(REQUEST.CONTENT_ARG, readmeBody));
			params.add(new BasicNameValuePair(REQUEST.COMMIT_MESSAGE_ARG, "init"));

			httpPost.setEntity(new UrlEncodedFormEntity(params));

			HttpResponse httpResponse = client.execute(httpPost);
			String json = EntityUtils.toString(httpResponse.getEntity());
			JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
			return gson.toJson(jsonObject);
		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}


